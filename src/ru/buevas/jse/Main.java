package ru.buevas.jse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import ru.buevas.jse.proxy.InvocationHandlerImpl;
import ru.buevas.jse.service.factorial.FactorialService;
import ru.buevas.jse.service.factorial.impl.FactorialServiceImpl;
import ru.buevas.jse.service.factorial.impl.TestFactorialServiceImpl;

public class Main {

    public static void main(String[] args) {
        FactorialServiceImpl factorialService = new FactorialServiceImpl();
        InvocationHandler handler = new InvocationHandlerImpl(factorialService);

        FactorialService proxy = (FactorialService) Proxy.newProxyInstance(
                factorialService.getClass().getClassLoader(), TestFactorialServiceImpl.class.getInterfaces(), handler);

        proxy.calculate(5);
        proxy.calculate(3);
        proxy.calculate(5);
        proxy.calculate(3);
    }
}
