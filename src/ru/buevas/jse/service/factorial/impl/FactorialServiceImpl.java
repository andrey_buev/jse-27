package ru.buevas.jse.service.factorial.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import ru.buevas.jse.service.factorial.FactorialService;

public class FactorialServiceImpl implements FactorialService {
    private static final Logger log = Logger.getLogger(FactorialServiceImpl.class.getName());

    public FactorialServiceImpl() {
    }

    @Override
    public Long calculate(Integer number) {
        log.log(Level.INFO, "Calculate factorial for {0}", number);
        if (number == 0) {
            return 1L;
        }
        return number * this.calculate(number - 1);
    }
}
