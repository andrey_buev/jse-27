package ru.buevas.jse.service.factorial.impl;

import java.util.logging.Level;
import java.util.logging.Logger;
import ru.buevas.jse.service.factorial.FactorialService;

public class TestFactorialServiceImpl implements FactorialService {
    private static final Logger log = Logger.getLogger(TestFactorialServiceImpl.class.getName());

    public TestFactorialServiceImpl() {
    }

    @Override
    public Long calculate(Integer number) {
        log.log(Level.INFO, "Calculate factorial for {0}", number);
        Long ret = 1L;
        for (int i = 1; i <= number; ++i) {
            ret *= i;
        }
        return ret;
    }
}
