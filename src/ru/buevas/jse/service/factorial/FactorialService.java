package ru.buevas.jse.service.factorial;

public interface FactorialService {
    Long calculate(Integer number);
}
