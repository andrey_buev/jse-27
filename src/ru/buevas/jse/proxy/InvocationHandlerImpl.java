package ru.buevas.jse.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.buevas.jse.service.factorial.impl.FactorialServiceImpl;

public class InvocationHandlerImpl implements InvocationHandler {
    private static final Logger log = Logger.getLogger(InvocationHandlerImpl.class.getName());
    private static final String PROXIED_METHOD = "calculate";

    private FactorialServiceImpl factorialService;
    private Map<Integer, Long> cache;

    public InvocationHandlerImpl(FactorialServiceImpl factorialService) {
        this.factorialService = factorialService;
        cache = new HashMap<>();
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals(PROXIED_METHOD)) {
            Integer number = (Integer) args[0];
            if (cache.containsKey(number)) {
                log.log(Level.INFO, "Factorial from cache for {0}", number);
                return cache.get(number);
            } else {
                return cache.put(number, (Long) method.invoke(factorialService, args));
            }
        } else {
            return method.invoke(factorialService, args);
        }
    }
}
